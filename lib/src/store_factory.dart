import 'executor.dart';
import 'reducer.dart';
import 'store.dart';

abstract class StoreFactory {
  Store<Intent, Event, State> create<Intent, Action, Result, State, Event>({
    String? name,
    required State initialState,
    required Executor<Intent, Action, Result, State, Event> Function() executorFactory,
    required Reducer<Result, State> reducer,
  });
}

extension SimplifiedCases on StoreFactory {
  Store<Intent, Event, State> createWithoutReducer<Intent, Action, State, Event>({
    String? name,
    required State initialState,
    required Executor<Intent, Action, State, State, Event> Function() executorFactory,
  }) =>
      create(
        name: name,
        initialState: initialState,
        executorFactory: executorFactory,
        reducer: (State state, _) => state,
      );

  BasicStore<Intent, State> createWithoutExecutor<Intent, State>({
    String? name,
    required State initialState,
    required Reducer<Intent, State> reducer,
  }) =>
      create(
        name: name,
        initialState: initialState,
        executorFactory: () => _BypassExecutor(),
        reducer: reducer,
      );
}

class _BypassExecutor<Intent, Result, State> with ExecutorMixin<Intent, void, Intent, State, void> {
  @override
  void handleIntent(Intent intent) => dispatch(intent);
}
