import '../executor.dart';
import '../logging/default_log_formatter.dart';
import '../logging/logging_reducer.dart';
import '../reducer.dart';
import '../store.dart';
import '../store_factory.dart';
import 'logging_executor.dart';

typedef LogFormatter = String? Function(String, StoreEventType, dynamic);
typedef LogHandler = void Function(String);
typedef Logger = void Function(StoreEventType, [dynamic]);

void defaultOutput(String s) => print(s);

class LoggingStoreFactory extends StoreFactory {
  final StoreFactory _delegate;
  final LogHandler output;
  final LogFormatter logFormatter;

  LoggingStoreFactory(this._delegate, {this.output = defaultOutput, LogFormatter? formatter})
      : logFormatter = formatter ?? DefaultLogFormatter().call;

  @override
  Store<Intent, Event, State> create<Intent, Action, Result, State, Event>({
    String? name,
    required State initialState,
    required Executor<Intent, Action, Result, State, Event> Function() executorFactory,
    required Reducer<Result, State> reducer,
  }) {
    if (name == null) {
      return _delegate.create(
        initialState: initialState,
        executorFactory: executorFactory,
        reducer: reducer,
      );
    } else {
      final Logger logger = (StoreEventType type, [dynamic value]) {
        final formatted = logFormatter(name, type, value);

        if (formatted != null) output(formatted);
      };

      return _delegate.create(
        name: name,
        initialState: initialState,
        executorFactory: () => LoggingExecutor(executorFactory(), logger),
        reducer: LoggingReducer(reducer, logger),
      );
    }
  }
}
