import '../executor.dart';
import '../store.dart';
import 'logging_store_factory.dart';

class LoggingExecutor<Intent, Action, Result, State, Event> with ExecutorMixin<Intent, Action, Result, State, Event> {
  final Executor<Intent, Action, Result, State, Event> delegate;
  final Logger logger;

  LoggingExecutor(this.delegate, this.logger);

  @override
  void dispose() {
    delegate.dispose();
    logger(StoreEventType.DISPOSED);
  }

  @override
  void init(
      State Function() stateGetter, Function(Result) onResult, Function(Event) onEvent, Function(Action) onAction) {
    logger(StoreEventType.STARTING, stateGetter());
    delegate.init(
      stateGetter,
      (result) {
        logger(StoreEventType.RESULT, result);
        onResult(result);
      },
      (event) {
        logger(StoreEventType.EVENT, event);
        onEvent(event);
      },
      onAction,
    );
  }

  @override
  void handleAction(Action action) {
    logger(StoreEventType.ACTION, action);
    delegate.handleAction(action);
  }

  @override
  void handleIntent(Intent intent) {
    logger(StoreEventType.INTENT, intent);
    delegate.handleIntent(intent);
  }
}
