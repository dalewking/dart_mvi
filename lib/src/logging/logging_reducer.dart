import '../reducer.dart';
import '../store.dart';
import 'logging_store_factory.dart';

class LoggingReducer<State, Result> extends ReducerClass<Result, State> {
  final Reducer<Result, State> delegate;
  final Logger logger;

  LoggingReducer(this.delegate, this.logger);

  @override
  State call(Result result, State state) {
    final State newState = delegate(result, state);

    logger(StoreEventType.STATE, newState);

    return newState;
  }
}
