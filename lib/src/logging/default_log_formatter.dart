import 'dart:math';

import '../store.dart';
import 'logging_store_factory.dart';

const int DEFAULT_VALUE_LENGTH_LIMIT = 256;

extension on String {
  String trimTo(int n) => substring(0, min(n, length));
}

extension on StoreEventType {
  String get description => toString().split('\.').last;
}

/// A default implementation of the [LogFormatter]
class DefaultLogFormatter {
  final int valueLengthLimit;

  const DefaultLogFormatter([this.valueLengthLimit = DEFAULT_VALUE_LENGTH_LIMIT]);

  String? call(String storeName, StoreEventType type, dynamic value) {
    if (value == null) {
      return "$storeName (${type.description})";
    } else {
      final valueText = valueLengthLimit <= 0 ? null : value?.toString().trimTo(valueLengthLimit);

      return "$storeName (${type.description})${valueText != null ? ": $valueText" : ""}";
    }
  }
}
