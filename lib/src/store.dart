import 'package:state_notifier/state_notifier.dart';

abstract class BasicStore<Intent, State> extends StateNotifier<State> {
  BasicStore(State initialState) : super(initialState);
  void call(Intent intent);
  void dispose();
  State get state => super.state;
}

abstract class Store<Intent, Event, State> extends BasicStore<Intent, State> {
  Store(State initialState) : super(initialState);

  void init();

  Stream<Event> get events;
}

enum StoreEventType { STARTING, INTENT, ACTION, RESULT, STATE, EVENT, DISPOSED }
