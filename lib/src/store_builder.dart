import 'executor.dart';
import 'reducer.dart';
import 'store.dart';
import 'store_factory.dart';

class StoreBuilder<State, Result> {
  final State initialState;
  final Reducer<Result, State> reducer;
  final String? name;

  StoreBuilder._(this.initialState, this.reducer, this.name);

  static State _bypassReducer<State>(State r, State _) => r;

  static StoreBuilder<State, State> forInitialState<State>(State initialState) =>
      StoreBuilder._(initialState, _bypassReducer, null);

  StoreBuilder<State, Result> withReducer<Result>(Reducer<Result, State> reducer) =>
      StoreBuilder._(initialState, reducer, name);

  StoreBuilder<State, Result> named(String? name) => StoreBuilder._(initialState, reducer, name);

  Store<Intent, Event, State> buildWithEvents<Intent, Event, Action>(
    StoreFactory factory,
    Executor<Intent, Action, Result, State, Event> Function() executorFactory,
  ) =>
      factory.create(
        name: name,
        initialState: initialState,
        executorFactory: executorFactory,
        reducer: reducer,
      );

  BasicStore<Intent, State> buildWithoutEvents<Intent, Action>(
    StoreFactory factory,
    Executor<Intent, Action, Result, State, void> Function() executorFactory,
  ) =>
      buildWithEvents(factory, executorFactory);

  BasicStore<Result, State> buildWithoutExecutor(StoreFactory factory) =>
      factory.createWithoutExecutor(initialState: initialState, reducer: reducer);
}
