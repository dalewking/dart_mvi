import 'package:meta/meta.dart';

/// Executor` is the place for business logic.
/// It accepts `Intents` and `Actions` and produces `Results` and `Events`.
abstract class Executor<Intent, Action, Result, State, Event> {
  void init(
    State Function() stateGetter,
    Function(Result) onResult,
    Function(Event) onEvent,
    Function(Action) onAction,
  );

  @protected
  State get state;

  @protected
  void Function(Result result) get dispatch;

  @protected
  void Function(Event event) get publish;

  @protected
  void Function(Action action) get perform;

  /// Called by the [Store] for every received `Intent`
  void handleIntent(Intent intent);

  /// Called by the [Store] for every `Action` produced by the [Bootstrapper]
  void handleAction(Action action);

  /// Disposes the [Executor], called by the [Store] when disposed
  void dispose();
}

abstract class ExecutorMixin<Intent, Action, Result, State, Event>
    implements Executor<Intent, Action, Result, State, Event> {
  late final State Function() _stateGetter;

  @protected
  State get state => _stateGetter();

  @protected
  late final void Function(Result result) dispatch;

  @protected
  late final void Function(Event event) publish;

  @protected
  late final void Function(Action action) perform;

  @protected
  void start() {}

  void init(
      State Function() stateGetter, Function(Result) onResult, Function(Event) onEvent, Function(Action) onAction) {
    _stateGetter = stateGetter;
    dispatch = onResult;
    publish = onEvent;
    perform = onAction;

    start();
  }

  /// Called by the [Store] for every received `Intent`
  void handleIntent(Intent intent) {}

  /// Called by the [Store] for every `Action` produced by the [Bootstrapper]
  void handleAction(Action action) {}

  /// Disposes the [Executor], called by the [Store] when disposed
  void dispose() {}
}
