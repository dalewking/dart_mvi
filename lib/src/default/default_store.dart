import 'dart:async';

import '../executor.dart';
import '../reducer.dart';
import '../store.dart';

class DefaultStore<Intent, Action, Result, State, Event> extends Store<Intent, Event, State> {
  final Executor<Intent, Action, Result, State, Event> executor;
  final Reducer<Result, State> reducer;
  final StreamController<Action> _actionController = StreamController();
  final StreamController<Event> _eventController = StreamController.broadcast();
  final StreamController<Intent> _intentController = StreamController();

  @override
  Stream<Event> get events => _eventController.stream;

  DefaultStore(
    State initialState,
    Executor<Intent, Action, Result, State, Event> Function() executorFactory,
    this.reducer,
  )   : executor = executorFactory(),
        super(initialState);

  void init() {
    if (mounted && !_intentController.hasListener) {
      executor.init(() => state, _changeState, _eventController.add, _actionController.add);

      _intentController.stream.forEach(executor.handleIntent);
      _actionController.stream.forEach(executor.handleAction);
    }
  }

  void _changeState(Result result) {
    if (mounted) state = reducer(result, state);
  }

  @override
  void call(Intent intent) {
    _intentController.add(intent);
  }

  @override
  void dispose() {
    if (mounted) {
      _intentController.close();
      _actionController.close();
      super.dispose();
      executor.dispose();
      _eventController.close();
    }
  }
}
