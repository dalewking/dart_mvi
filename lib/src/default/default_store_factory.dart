import '../default/default_store.dart';
import '../executor.dart';
import '../reducer.dart';
import '../store.dart';
import '../store_factory.dart';

class DefaultStoreFactory extends StoreFactory {
  @override
  Store<Intent, Event, State> create<Intent, Action, Result, State, Event>({
    String? name,
    bool autoInit = true,
    required State initialState,
    required Executor<Intent, Action, Result, State, Event> Function()
        executorFactory,
    required Reducer<Result, State> reducer,
  }) {
    var store = DefaultStore(
      initialState,
      executorFactory,
      reducer,
    );

    if (autoInit) store.init();

    return store;
  }
}
