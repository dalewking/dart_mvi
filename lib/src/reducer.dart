typedef Reducer<Result, State> = State Function(Result result, State state);

abstract class ReducerClass<Result, State> {
  State call(Result result, State state);
}
